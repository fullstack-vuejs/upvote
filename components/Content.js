const Content={
    template:
    `
    <div>
    <v-row>
        <v-col cols="12" xs="12" md="3" sm="3" lg="3" xl="3" />
        <v-col cols="12" xs="12" md="6" sm="6" lg="6" xl="6" ><card-vue v-for="item in sortedItems" :key="item.id" :title="item.title" :avatar="item.avatar" :desc="item.desc":submission="item.submissions" @onHappy="updateVote(item.id)" :avatar="item.avatar" :id="item.id"  :vote="item.vote"></card-vue></v-col>
        <v-col cols="12" xs="12" md="3" sm="3" lg="3" xl="3" />
        
    </v-row>
    </div>
    `,
    components:{
        'card-vue':Card
    },
    data(){
        return {
            items:[
                {id:1,title:'Tinfoild:Tailored tinfoil hats',desc:'We have your measurement and shipping address.',avatar:'images/avatars/daniel.jpg',submissions:'images/submissions/image-steel.png',vote:17},
                {id:2,title:'Yellow Pail',desc:'On-demand sand castle construction expertise.',avatar:'images/avatars/kristy.png',submissions:'images/submissions/image-yellow.png',vote:16},
                {id:3,title:'Supermajority:The Fantasy Congress League',desc:'Earn points whrn your politician passess legislation',avatar:'images/avatars/molly.png',submissions:'images/submissions/image-aqua.png',vote:11},
                {id:4,title:'Haught or Naught',desc:'High-minded or absent-minded.',avatar:'images/avatars/veronika.jpg',submissions:'images/submissions/image-rose.png',vote:9}
            ]
        }
    },
    computed:{
        sortedItems(){
            return this.items.sort((a,b)=>{
                return b.vote-a.vote;
            })
        }
    },
    methods:{
        updateVote(itemId){
            const itemz=this.items.find(
                itemz=>itemz.id===itemId
            )
            itemz.vote++
        }
    }
}