const Card={
    template:
    `
    <v-card class="pa-3" style="margin-bottom:14px">
        <v-row no-gutters>
            <v-col cols="2" xs="2" sm="2" >
            <v-avatar
            tile
            color="indigo"
            size="50"
          >
            <v-img :src="submission"></v-img>
          </v-avatar>
            </v-col>

            <v-col cols="8" xs="8" sm="9" >
                <p class="font-weight-bold " style="line-height:1;margin-top:5px"> {{title}}<span style="margin-left:2px;font-size:11px"class="grey">#{{id}}</span></p>
                <p class="text-body-2" style="line-height:1;margin-top:-14px">{{desc}}</p>
                <p class="" style=";margin-top:-14px;font-size:12px">submitted by <v-avatar
                
               
                size="20"
              >
                <v-img :src="avatar"></v-img>
              </v-avatar> </p>
            </v-col>
          
            <v-col cols="2" xs="2" sm="1"  align="right" @click="execute">
                {{vote}}<v-icon>keyboard_arrow_up</v-icon>
            </v-col>
        </v-row>
    </v-card>
    `,
    // props:['title','id','desc','submission','vote',''],
    props:{
        title:String,
        id:Number,
        desc:String,
        submission:String,
        vote:Number,
        avatar:String
       
    },
    methods: {
        execute() {
          // ... do something here
          this.$emit('onHappy')
        }
      }

    
}